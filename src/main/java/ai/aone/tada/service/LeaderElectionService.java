package ai.aone.tada.service;

import java.io.IOException;
import java.util.List;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import lombok.Getter;

@Service
public class LeaderElectionService implements Watcher{
    private static final Logger logger = LoggerFactory.getLogger(LeaderElectionService.class);

    private static final String LEADER_ELECTION_NODE = "/leader";
    private static final String SERVER_HEALTH_NODE = "/server";
    
    // @Autowired
    private ZooKeeper zooKeeper;

    @Value("${zookeeper.url}")
    @Getter
    private String url;

    @Getter
    @Value("${server.port}")
    private String serverPort;

    private String serverName;

    private String serverPath;

    private String getServerName() {
        logger.info("Getting server name...");
        if(this.serverName != null) { return this.serverName; }
        this.serverName = String.format("server-%s", getServerPort());
        return this.serverName;
    }

    private String getServerPath() {
        logger.info("Getting server path...");
        if(this.serverPath != null) { return this.serverPath; }
        this.serverPath = String.format("%s/%s",SERVER_HEALTH_NODE,getServerName());
        return this.serverPath;
    }

    @Autowired
    private TaskService taskService;

    @PostConstruct
    public void start() throws IOException, KeeperException, InterruptedException {
        logger.info("Starting...");
        this.zooKeeper = new ZooKeeper(getUrl(), 3000, this);
        registerService();
        register();
        volunteer();
        watch();
    }

    @PreDestroy
    public void stop() throws InterruptedException {
        logger.info("Stopping...");
        if(this.zooKeeper != null) {
            this.zooKeeper.close();
        }
    }

    public void registerService() throws KeeperException, InterruptedException {
        logger.info("Registering Service...");
        Stat isUpAlready = zooKeeper.exists(SERVER_HEALTH_NODE, false);
        logger.info("isUpAlready:{}",isUpAlready);
        if(isUpAlready != null) {
            logger.info("Exiting...");
            return;
        }
        String path = zooKeeper.create(
            SERVER_HEALTH_NODE,
            getServerPort().getBytes(),
            ZooDefs.Ids.OPEN_ACL_UNSAFE,
            CreateMode.PERSISTENT
        );
        logger.info("Registered serverName:{} at path:{} returned:{}",SERVER_HEALTH_NODE,getServerPath(),path);
    }

    public void register() throws KeeperException, InterruptedException {
        logger.info("Registering...");
        String path = zooKeeper.create(
            getServerPath(),
            getServerPort().getBytes(),
            ZooDefs.Ids.OPEN_ACL_UNSAFE,
            CreateMode.EPHEMERAL
        );
        logger.info("Registered serverName:{} at path:{} returned:{}",getServerName(),getServerPath(),path);
    }

    public void volunteer() throws InterruptedException {
        logger.info("Volunteering...");
        try {
            String path = zooKeeper.create(
                LEADER_ELECTION_NODE,
                getServerPort().getBytes(),
                ZooDefs.Ids.OPEN_ACL_UNSAFE,
                CreateMode.EPHEMERAL
            );
            logger.info("Leader elected:{}.",path);
        } catch (KeeperException e) {
            logger.info("Leader already exists:{}",e);
        }
    }

    public void watch() throws KeeperException, InterruptedException {
        logger.info("Watching...");
        Stat stat = zooKeeper.exists(LEADER_ELECTION_NODE, true);
        logger.info("Watch stat:{}",stat);
        if(stat != null) {
            byte[] data = zooKeeper.getData(LEADER_ELECTION_NODE,this, stat);
            logger.info("Current leader:{}",new String(data));
        } else {
            logger.info("No leader to watch.");
        }
    }

    public void process(WatchedEvent event) {
        logger.info("Processing...");
        try {
            if(event.getType() == Event.EventType.NodeDeleted && event.getPath().equals(LEADER_ELECTION_NODE)) {
            logger.info("Leader deleted.");
                volunteer();
                watch();//might not be needed just putting to check current leader
                assign();//i should call only if i am the leader
                return;
            }
            if(event.getType() == Event.EventType.NodeChildrenChanged && event.getPath().equals(SERVER_HEALTH_NODE)) {
                logger.info("Children Node changed.");
                watch();//might not be needed just putting to check current leader
                assign();//i should call only if i am the leader
            }
        } catch (InterruptedException | KeeperException e) {
            logger.info("Exception while processing:{}",e);
        }
    }

    private void assign() throws KeeperException, InterruptedException {
        List<String> servers = zooKeeper.getChildren(SERVER_HEALTH_NODE,this);
        for(int index = 0; index < servers.size(); index++) {
            String server = servers.get(index);
            logger.info("Assigned task:{} to server:{}.",index,server);
            taskService.complete(index);
        }
    }

}
