package ai.aone.tada.service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import lombok.Getter;

@Getter
@Service
public class TaskService {
    private static final Logger logger = LoggerFactory.getLogger(TaskService.class);
    private List<Consumer<Integer>> tasks;
    private static int totalTaskCount = 5;

    public TaskService() {
        logger.info("Initializing...");
        tasks = new ArrayList<>();
        while(totalTaskCount-->0) {
            this.add();
        }
    }

    public void add() {
        logger.info("Adding...");
        int taskId = tasks.size();
        tasks.add(o->{
            logger.info("Finished task:{}",taskId);
        });
    }

    public void complete(Integer taskIndex) {
        logger.info("Completing...");
        taskIndex %= tasks.size();
        tasks.get(taskIndex).accept(taskIndex);
    }
}
